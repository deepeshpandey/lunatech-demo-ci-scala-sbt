import org.scalatest._

class FibonacciSpec extends FlatSpec with Matchers {
  "Fibonacci" should "calculate the 10th Fibonacci number correctly" in {
    Fibonacci.fib(10) should === (55)
  }
}
